//
//  ViewController.swift
//  pageTheScroll
//
//  Created by Mustafa Khalil on 1/25/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var scrollView: UIScrollView!
    var currentPage = 0
    var maxPage = 2
    var minPage = 0

    var imageArr = [UIImageView]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
                
    }
    override func viewDidAppear(_ animated: Bool) {
        
        var content: CGFloat = 0.0
        
        let scrollX = scrollView.frame.size.width
        for x in 0...2{
            let image = UIImage(named: "icon\(x).png")
            let imageView = UIImageView(image: image)
            imageArr.append(imageView)
            
            var newX: CGFloat  = 0.0
            newX = scrollX/2 + scrollX * CGFloat(x)
            
            content += newX
            
            scrollView.addSubview(imageView)
            imageView.frame = CGRect(x: newX - 75, y: (scrollView.frame.size.height/2)-75, width: 150, height: 150)
            
            
            
        }
        
        scrollView.clipsToBounds = false
        scrollView.contentSize = CGSize(width: content, height: view.frame.size.height)

    }
    @IBAction func detectSwipeRight(_ sender: Any) {
        if (currentPage > minPage && (sender as AnyObject).direction == UISwipeGestureRecognizerDirection.right){
            moveScrollView(-1)
        }
    }
    
    @IBAction func detectSwipeLeft(_ sender: Any) {
        if (currentPage < maxPage && (sender as AnyObject).direction == UISwipeGestureRecognizerDirection.left){
            moveScrollView(1)
        }
    }

    
    func moveScrollView(_ dircetion: Int){
        currentPage = currentPage + dircetion
        let point: CGPoint = CGPoint(x: scrollView.frame.size.width * CGFloat(currentPage), y: 0.0)
        scrollView.setContentOffset(point, animated: true)

    }


}

